/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import gta.model.Box;
import gta.model.Heart;

/**
 *
 * @author homolja1
 */
public interface WorldListener {
    public void onCrashBox(Box box);
    
    public void onCatchHeart(Heart heart);
    
}
