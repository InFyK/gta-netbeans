package gta.model;

import java.awt.geom.Rectangle2D;

/**
 *
 * @author homolja1
 */

public class Heart extends GameObject{
         private Rectangle2D.Float rectangle;
         private float width;
         private float height;
	public Heart(float x, float y) {
		super(x,y);
                
                this.width = 1f;
                this.height = 1f;
	 this.rectangle = new Rectangle2D.Float(x, y, width, height);
        }
	
	@Override
	public String toString() {
		return "Heart [" + this.x + ", " + this.y + "]";
	}

        public Rectangle2D.Float getRectangle(){
        this.rectangle.x = x;
        this.rectangle.y = y;
        
        return this.rectangle;
        }
	@Override
	public void update() {
		this.y -= -4f;	
		if(this.y>720f)
		{
			this.y+=-700f;
			this.x=0;
			while(x<115 || x>870)
			{						
				this.x=((float)Math.random() * 1024);
			}
		}	
	}
public boolean collidesWith(Rectangle2D.Float otherRectangle){
    return this.rectangle.intersects(otherRectangle);
 }
}
