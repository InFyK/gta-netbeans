package gta.model;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;

/**
 *
 * @author homolja1
 */


public class Car extends GameObject{
    
    protected float speed=1f;
    private String name;
    private float width;
    private float height;
    private Rectangle2D.Float rectangle;
    public Car(String name, float x, float y) {
        super(x, y);
        this.name = name;
        this.width = 2f;
        this.height = 4f;
        rectangle = new Rectangle2D.Float(x, y, width, height);
       
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "CAR [" + this.x + ", " + this.y + "]";
    }

    @Override
    public void update() {
        //this.x-=5;
        
        
        
    }
    

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
public void moveRight() {
    this.x+=12f;
}
public void moveLeft() {
    this.x-=12f;
}

public void speedUp() {
    speed+=0.5f;
}
public void speedDown() {
    if(speed>0.5f){
    speed-=0.5f;
    }
}
public float getSpeed() {
    return speed;
}
 public boolean collidesWith(Rectangle2D.Float otherRectangle){
     this.rectangle.x = x;
     this.rectangle.y = y;
    return this.rectangle.intersects(otherRectangle);
 }
}