package gta.model;

import java.awt.geom.Rectangle2D;

/**
 *
 * @author homolja1
 */

public class Box extends GameObject {    
    
    
    
    private Rectangle2D.Float rectangle;
    private float width;
    private float height;
     
      
	
	
	public Box(float x, float y) {
	this(x, y, 1 );
        this.width = 35;
        this.height = 35;
        this.rectangle = new Rectangle2D.Float(x, y, width, height);
	}
	
	public Box(float x, float y, int width) {
		super(x,y);
		this.width = width;
                
        }

	public float getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
	public Rectangle2D.Float getRectangle(){
        this.rectangle.x = x;
        this.rectangle.y = y;
        
        return this.rectangle;
        }
	@Override
	public String toString() {
		return "BOX [" + this.x + ", " + this.y + "]";
	}

	@Override
	public void update() {
		this.y -= -4f;	
		if(this.y>750f)
		{
			this.y+=-800f;
			this.x=0;
			while(x<115 || x>870)
			{						
				this.x=((float)Math.random() * 1024);
			}
		}
	}
	public boolean collidesWith(Rectangle2D.Float otherRectangle){
    return this.rectangle.intersects(otherRectangle);
 }

}
