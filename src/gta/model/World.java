package gta.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import gta.gui.GameCanvas;
import interfaces.WorldListener;
import java.awt.event.WindowEvent;

public class World {

	private Car car;
	private List<Box> boxes;
	private List<Heart> hearts;

	public static final float WIDTH = 1024f;
	public static final float LENGHT = 500f;
        
        private WorldListener worldListener;
        public int life = 50000;
	public World(Car car) {
		this.car = car;
		boxes = new ArrayList<Box>();
		hearts = new ArrayList<Heart>();
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public List<Box> getBoxes() {
		return boxes;
	}

	public List<Heart> getHearts() {
		return hearts;
	}

	public void addBox(Box box) {
		this.boxes.add(box);
	}

	public void addHeart(Heart heart) {
		this.hearts.add(heart);
	}

	private void addBox(int i, int j) {
		// TODO Auto-generated method stub

	}

	private float randomX(){
		return (float) Math.random()*WIDTH;
	}
	private float randomY(){
		return (float) Math.random()*LENGHT;
	}
 
	public void init() {
		int boxes = 5;
		int hearts = 1;
		double	x = 0;
		double	y = 0;
		double[] checkX = new double[boxes]; 
		double[] checkY = new double[boxes];

		for (int i = 0; i < boxes; i++) {
			x = 0;
			y = 0;	
			
			while(x<0f || x>1024f)
			{
				x = randomX();
			}		
			while(y<1f || y>500f)
			{
				checkY[i] = (Math.random() * WIDTH);
				y = randomY();
			}			
		}
		for (int i = 0; i < boxes; i++) {
			x = 0;
			y = 0;			
			
		for (int ii = 1; ii < boxes; ii++)
		{						
		if(checkX[ii]>checkX[i]-40 && checkX[ii]<checkX[i]+80)
		{
		while(x<0f || x>1024f && checkX[ii]>checkX[i]-40 && checkX[ii]<checkX[i]+75)
		{
		checkX[i] = (Math.random() * WIDTH);
		x = checkX[i];
		}
		}	
		if(checkY[ii]>checkY[i]-40 && checkY[ii]<checkY[i]+80)
		{
		while(y<0f || y>500f && checkY[ii]>checkY[i]-40 && checkY[ii]<checkY[i]+75)
		{
		checkY[i] = (Math.random() * WIDTH);
		y = checkY[i];
		}
		}	
		}						
		}
		for(int i = 0; i < boxes; i++){
			x=checkX[i];y=checkY[i];
			addBox(new Box((float) x, (float) y));
		}

		for (int i = 0; i < hearts; i++) {
			x = 0;
			y = 0;
			while(x<0f || x>1024f)
			{
				x = (Math.random() * WIDTH);
			}
			y = Math.random() * LENGHT;
			addHeart(new Heart((float) x, (float) y));
		}
	}
public int score = 0;
    public void update() {
            float speed = car.getSpeed();
           score=(int) (score + speed);
            car.update();
            
            
		for (Box box : boxes) {
			box.update();
                        box.y+=speed;
		if (car.collidesWith(box.getRectangle())){
                    life--;
                System.out.println("Minus zivot");
                
                box.x=-100;
                worldListener.onCrashBox(box);
                }
                }
		for (Heart heart : hearts) {
			heart.update();
                        heart.y+=speed;
                        if (car.collidesWith(heart.getRectangle())){
                            life++;
                            
                System.out.println("Plus zivot");
                heart.x=-100;
                worldListener.onCatchHeart(heart);
                }
		}
	}

    public void addListener(WorldListener worldListener){
    this.worldListener = worldListener;
    }
    
@Override
public String toString() {
    String text = "";
		text += car;
		for (Box box : boxes) {
			text += box;
		}
		for (Heart heart : hearts) {
			text += heart;
		}
		return text;
	}

}
