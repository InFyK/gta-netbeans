package gta;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;


import gta.gui.MainFrame;
import gta.model.Box;
import gta.model.Car;
import gta.model.Heart;
import gta.model.World;

public class App {

	public static void main(String[] args) {
		
		Car car = new Car("Moje auto",10f, 10f);		
		World world = new World(car);
		
		world.init();		
		
		MainFrame mainFrame = new MainFrame(world);
		
		TimerTask timerTask = new TimerTask() {		
			
			@Override
			public void run() {
				world.update();
				mainFrame.revalidate();
			}
		};
		
		Timer timer = new Timer();
		timer.schedule(timerTask, 0, 10);				             
        }

}
