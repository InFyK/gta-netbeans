package gta.gui;


import gta.model.Box;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import gta.model.Car;
import gta.model.Heart;
import gta.model.World;
import interfaces.WorldListener;
import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MainFrame extends JFrame implements KeyListener, WorldListener {
	
	private GameCanvas gameCanvas;
	World world;
        Car car;
        float speed;
        JLabel labelSpeed;
        public float getSpeed = speed;
        private static final int ROAD_WIDTH = 800;
        private boolean pressedLeft = false;
        private boolean pressedRight = false;
        private boolean pressedUp = false;
        private boolean pressedDown = false;
	
        
        public MainFrame(World world) {
        super();
        this.world = world;
        this.world.addListener(this);
        
        setLayout(new BorderLayout());
        labelSpeed = new JLabel();
        add(labelSpeed, BorderLayout.NORTH);
        addKeyListener(this);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Game");
        setSize(1024, 768);
        gameCanvas = new GameCanvas(world);
        add(gameCanvas);
        
        setVisible(true);
    }
	
	@Override
    public void revalidate() {
        car = world.getCar();
        speed = car.getSpeed();
        labelSpeed.setText("Speed: "+speed*10);
        //car movement
        if(pressedLeft==true) {
            car.moveLeft();
        }
        if(pressedRight==true){
            car.moveRight();
        }
        if(pressedUp==true) {
            car.speedUp();
        }
        if(pressedDown==true) {
            car.speedDown();
        }
        super.revalidate();
        repaint();
    }
@Override
    public void keyReleased(KeyEvent e) {
        
        //LEFT key released
        if(e.getKeyCode() == KeyEvent.VK_LEFT) {
            pressedLeft = false;
            
        }
        
        //RIGHT key released
        if(e.getKeyCode()== KeyEvent.VK_RIGHT) {
            pressedRight = false;
        }
        //UP key released
        if(e.getKeyCode() == KeyEvent.VK_UP) {
            pressedUp = false;
            
        }
        
        //DOWN key released
        if(e.getKeyCode()== KeyEvent.VK_DOWN) {
            pressedDown = false;
        }
    }

@Override
    public void keyPressed(KeyEvent e) {
        
        //LEFT key pressed
       if(e.getKeyCode() == KeyEvent.VK_LEFT) {
            pressedLeft = true;
        }
       //RIGHT key pressed
        if(e.getKeyCode()== KeyEvent.VK_RIGHT) {
            pressedRight = true;
        }
         //UP key pressed
       if(e.getKeyCode() == KeyEvent.VK_UP) {
            pressedUp = true;  
        }
       //DOWN key pressed
        if(e.getKeyCode()== KeyEvent.VK_DOWN) {
            pressedDown = true;
        }
    }
    @Override
    public void keyTyped(KeyEvent e) {        
    }

    @Override
    public void onCrashBox(Box box) {
       System.out.println("CRASH with " + box.toString()); //To change body of generated methods, choose Tools | Templates.
    int life = world.life;
       if (life <1){
           JOptionPane.showMessageDialog(null, "GAMEOVER");
           
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
            
            }
    }

    @Override
    public void onCatchHeart(Heart heart) {
        System.out.println("CRASH with " + heart.toString()); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
