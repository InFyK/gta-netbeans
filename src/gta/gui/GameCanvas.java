package gta.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JPanel;

import gta.model.Box;
import gta.model.Car;
import gta.model.Heart;
import gta.model.World;
import java.awt.Font;

public class GameCanvas extends JPanel {

	private World world;
	
	private static final int ROAD_WIDTH = 800;
	
	public GameCanvas(World world) {
		super();
		this.world = world;
	}
	
        
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		Color brown = new Color(205,133,63);
		Color darkGreen = new Color(0,150,0);
		
		int roadStartX = (getWidth() - ROAD_WIDTH)/2;
		float ratioX = ROAD_WIDTH / World.WIDTH;
		float ratioY = ratioX;
		
		// grass
		g.setColor(darkGreen);
		g.fillRect(0,0,getWidth(),getHeight());
		// road
		g.setColor(Color.BLACK);
		g.fillRect(roadStartX,0,ROAD_WIDTH,getHeight());	//TODO metody
		g.setColor(Color.WHITE);
		
		//boxes
		List<Box> boxes = world.getBoxes();		
		for(Box box : boxes) {			
		g.setColor(brown);
		g.fillRect(roadStartX + (int)(box.getX()*ratioX),(int) (box.getY()*ratioY), 35, 35);
		}
		//hearts
		List<Heart> hearts = world.getHearts();
		for(Heart heart : hearts) {
		g.setColor(Color.RED);
		g.fillOval(roadStartX + (int)(heart.getX()*ratioX),(int) (heart.getY()*ratioY), 25, 25);
		}		
		//car
		Car car = world.getCar();
		g.setColor(Color.white);
		g.fillRect((roadStartX+(int)(car.getX()*ratioX)),(getHeight()+(int) (car.getY()*ratioY)-90), 40, 80);
	
                int life = world.life;
                g.setColor(Color.red);
                g.setFont(new Font("arial", Font.PLAIN, 16));
                g.drawString("Life: " + life, 5, 55);
                
                //score
                float speed = car.getSpeed();
                float score = world.score/20;
                
                g.setColor(Color.white);
                g.setFont(new Font("arial", Font.PLAIN, 16));
                g.drawString("Score: " + score, 5, 30);
        }
	
	/*
	public boolean isOutOfWorld(){
	return getY()<0;
	}*/
	public int halfOfRoadWidth(){
		return (ROAD_WIDTH/2);
	}

	}

